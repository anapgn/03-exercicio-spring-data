package com.itau.funcionarios.repositories;
import org.springframework.data.repository.CrudRepository;

import com.itau.funcionarios.models.Localizacao;

public interface LocalizacaoRepository extends CrudRepository<Localizacao, Integer> {

}
